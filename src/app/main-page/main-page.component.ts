import { Reservation } from '../backend-gateway/model/reserve';
import { Course } from './../backend-gateway/model/course';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CourseService } from '../backend-gateway/course-service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  course: Course
  name = new FormControl('');
  email = new FormControl('');

  reserve = <Reservation>{}

  constructor(public courseService: CourseService, private router: Router) {

    this.course = new Course( "1", "A-CSD", "28-9-2021", 5)
  }

  ngOnInit(): void {

    this.courseService.createCourse(this.course).subscribe(() => {
      console.log("Treinamento criado")
    })

  }

  reservarVaga() {

    this.reserve.studentName = this.name.value
    this.reserve.studentEmail = this.email.value
    this.reserve.courseId = this.course.courseId

    this.courseService.reserveSeat(this.reserve).subscribe(result => {

      console.log("Vaga Reservada! " + result)

      return this.router.navigateByUrl('/thankyou')

    },
    (error:HttpErrorResponse) => {
      alert(error.message)
    })

    console.log("Reserva: " + JSON.stringify(this.reserve))
  }
}
