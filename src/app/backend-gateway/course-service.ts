import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Course } from './model/course';
import { Reservation } from './model/reserve';

@ Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) {
  }

  reserveSeat(reservation: Reservation) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.post<Comment>(
      "http://localhost:8080/api/course/reserve",
      reservation,
      httpOptions
    )
  }

  createCourse(course: Course) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.post<Comment>(
      "http://localhost:8080/api/course/create",
      course,
      httpOptions
    )
  }
}
