export class ReservationResponse {

  public reservationId: string;

  constructor(reservationId:string) {
      this.reservationId = reservationId;
  }

}
