export class Reservation {
  public studentName: string
  public studentEmail: string
  public courseId: string

  constructor(studentName: string,  studentEmail: string, courseId: string) {
    this.studentName = studentName
    this.studentEmail = studentEmail
    this.courseId = courseId
  }

}
