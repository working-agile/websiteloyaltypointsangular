export class Course {
  public name: string
  public courseId: string
  public date: string
  public numberOfSeats: number

  constructor(id: string, nome: string, dataInicio: string, numeroVagasEmAberto: number) {
    this.name = nome
    this.courseId = id
    this.date = dataInicio
    this.numberOfSeats = numeroVagasEmAberto
  }

}
