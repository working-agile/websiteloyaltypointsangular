import { ThankYouPageComponent } from './thank-you-page/thank-you-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';

const routes: Routes = [
  { path: "", component: MainPageComponent },
  { path: "thankyou", component: ThankYouPageComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })], exports: [RouterModule]
})
export class AppRoutingModule { }
